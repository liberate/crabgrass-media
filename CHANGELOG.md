<a name="0.5.0"></a>
### 0.5.0 (2021-12-27)


#### Upgrade

* require rails 6
* use rake 13


<a name="0.4.0"></a>
### 0.4.0 (2021-12-26)


#### Bug Fixes

* use inkscape cli options for version >=1.0	 ([1cb8671](/../../commit/1cb8671))


<a name="0.3.1"></a>
### 0.3.1 (2019-04-18)


#### Bug Fixes

* libre office to use uniq UserInstallation	 ([5916b32](/../commit/5916b32))


<a name="0.3.0"></a>
### 0.3.0 (2018-06-12)


#### Upgrade

* require rails 5.x


<a name="0.2.0"></a>
### 0.2.0 (2017-02-23)


#### Upgrade

* require rails 4.2


#### Bug Fixes

* fix all warnings from gem build	 ([9add68a](/../commit/9add68a))
* warnings during test run	 ([245564f](/../commit/245564f))
* duplicate keys in mime type hash	 ([131cb9b](/../commit/131cb9b))


<a name="0.1.1"></a>
### 0.1.1 (2017-02-24)


#### Bug Fixes

* prevent raising ENOENT on failure	 ([dfb0baf](/../commit/dfb0baf))


<a name="0.1.0"></a>
### 0.1.0 (2017-02-24)


#### Features

* use new class names for crabgrass 0.7	 ([909295f](/../commit/909295f))



